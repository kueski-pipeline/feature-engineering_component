from kfp.dsl import ContainerOp
from kubernetes.client import V1EnvVar

from kueski_feature_engineering.constants import S3_ENDPOINT, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY


def setup_resources(operator: ContainerOp, memory_request: int, memory_limit: int,
                    cpu_request: int, cpu_limit: int) -> ContainerOp:

    operator.container\
        .set_memory_request(memory_request)\
        .set_memory_limit(memory_limit)\
        .set_cpu_request(cpu_request)\
        .set_cpu_limit(cpu_limit)

    return operator


def add_environments(operator: ContainerOp) -> ContainerOp:
    operator.container\
        .add_env_variable(env_variable=V1EnvVar(name="S3_ENDPOINT", value=S3_ENDPOINT)) \
        .add_env_variable(env_variable=V1EnvVar(name="AWS_ACCESS_KEY_ID", value=AWS_ACCESS_KEY_ID)) \
        .add_env_variable(env_variable=V1EnvVar(name="AWS_SECRET_ACCESS_KEY", value=AWS_SECRET_ACCESS_KEY))

    return operator
