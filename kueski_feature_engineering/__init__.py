from kfp import dsl
from kfp.dsl import ContainerOp

from kueski_feature_engineering.configure import setup_resources, add_environments
from kueski_feature_engineering.constants import DEFAULT_MAX_RECORDS_PER_FILE, DEFAULT_SPARK_MASTER, STARTUP_COMMAND, IMAGE_NAME,\
    IMAGE_TAG, CPU_LIMIT, CPU_REQUEST, MEMORY_LIMIT, DISPLAY_NAME, MEMORY_REQUEST


@dsl.component
def make_kueski_feature_engineering(
        source_glob: str = "", training_output_folder: str = "", serving_output_folder: str = "",
        spark_master: str = DEFAULT_SPARK_MASTER, max_records_per_file: int = DEFAULT_MAX_RECORDS_PER_FILE,

        command: str = STARTUP_COMMAND, image_name: str = IMAGE_NAME, image_tag: str = IMAGE_TAG,
        display_name: str = DISPLAY_NAME, memory_request: int = MEMORY_REQUEST,
        memory_limit: int = MEMORY_LIMIT, cpu_request: int = CPU_REQUEST, cpu_limit: int = CPU_LIMIT):

    image = f"{image_name}:{image_tag}"
    operator = ContainerOp(
        image=image,
        name="main",
        arguments=[
            "--source-glob", source_glob,
            "--training-output-folder", training_output_folder,
            "--serving-output-folder", serving_output_folder,
            "--spark-master", spark_master,
            "--max-records-per-file", max_records_per_file
        ]
    )

    operator = operator.set_display_name(display_name)
    operator = setup_resources(operator, memory_request, memory_limit, cpu_request, cpu_limit)
    operator = add_environments(operator)
    operator.execution_options.caching_strategy.max_cache_staleness = "P0D"

    return operator
