STARTUP_COMMAND = "import-files"
IMAGE_NAME = "andreclaudino/kueski-feature-engineering"
IMAGE_TAG = "1.0.0"


DISPLAY_NAME = "Run feature engineering"

MEMORY_REQUEST = "3Gi"
MEMORY_LIMIT = "8Gi"

CPU_REQUEST = "200m"
CPU_LIMIT = "4000m"

LOGLEVEL = "debug"
S3_ENDPOINT = "http://minio.10.0.0.200.nip.io:80"
AWS_ACCESS_KEY_ID = "minio"
AWS_SECRET_ACCESS_KEY = "minio123"


DEFAULT_SPARK_MASTER = "local[*]"
DEFAULT_MAX_RECORDS_PER_FILE = 10